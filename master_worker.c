#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <octave/oct.h>
#include <octave/octave.h>
#include <octave/parse.h>
// Library for old versions of Octave
#include <octave/toplev.h> /* do_octave_atexit */
#include "comunes.c"

// Pseudocode for master worker
#define TAG_RESULT 0
#define TAG_ASK_FOR_JOB 1
#define TAG_JOB_DATA 2
#define TAG_STOP 3
#define BUFFER_LIMIT 128
#define FILE_ROW_LINE 128
#define MATRIX_LIMIT 10000

octave_value_list result_lcao;

void parse_string(char *inBuffer, int outBuffer[4])
{
    int count = 1;
    int length = strlen(inBuffer);

    char a[BUFFER_LIMIT], b[BUFFER_LIMIT], c[BUFFER_LIMIT], d[BUFFER_LIMIT];
    char buf[length];
    strcpy(buf, inBuffer);
    char delimiter[] = " ";
    
    char *token = strtok(buf, delimiter);
    while(token != NULL && count < 5){
        // Sólo en la primera pasamos la cadena; en las siguientes pasamos NULL
	switch (count)
        {
	    case 1:
   	        strcpy(a, token);
	    break;
            case 2:
                strcpy(b, token);
            break;
            case 3:
                strcpy(c, token);
            break;
            case 4:
                strcpy(d, token);
            break;
        }
        count++;
        token = strtok(NULL, delimiter);
    }
    
    outBuffer[0] = atoi(a);
    outBuffer[1] = atoi(b);
    outBuffer[2] = atoi(c);
    outBuffer[3] = atoi(d);
}

// for the master you have to provide code for managing individual tasks
// and for managing the workers with their tasks they are working on
void master(FILE *file, int num_workers, octave_value_list arguments_lcaomain, octave_value_list arguments_agua_finalizacion,
    octave_value_list arguments_save_times, octave_value_list result_cf, octave_value_list result_lcao)
{
    MPI_Status stat, stat2;

    file = fopen("sqp1", "r");
    char skip_buffer[FILE_ROW_LINE];
    char buffer[BUFFER_LIMIT] = "";
    int finished_workers = 0;
    int count_unsed_lines = 0;

    while (count_unsed_lines < 5)
    {
	fgets(skip_buffer, FILE_ROW_LINE, file);
        count_unsed_lines++;
    }

    octave_value_list result_lcaomain = feval ("lcaomain", arguments_lcaomain, 1);

    printf("Soy el Master\n");

    const char * md_name="md",
               * ldiv_name="ldiv",
               * sqp1_name="sqp1",
               * centros_name="centros";

    // Para guardar los tamaños de las matrices (bytes leidos del archivo en disco)
    long md_size, ldiv_size, centros_size;
    // Cargo las matrices para enviarlas a todos los workers
    char * md = load_matrix(md_name, &md_size);
    char * ldiv = load_matrix(ldiv_name, &ldiv_size);
    char * centros = load_matrix(centros_name, &centros_size);

    int cant_rows = 0;

    /* there are jobs unprocessed  || there are workers still working on jobs */
    while (finished_workers != num_workers)
    {
        // Wait for any incomming message
        MPI_Probe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);

        // Store rank of receiver into worker_rank
        int worker_rank = stat.MPI_SOURCE;

        // Decide according to the tag which type of message we have got
        if (stat.MPI_TAG == TAG_ASK_FOR_JOB)
        {
            MPI_Recv(buffer, BUFFER_LIMIT, MPI_CHAR, worker_rank, TAG_ASK_FOR_JOB, MPI_COMM_WORLD, &stat2);
	    memset(buffer, 0, BUFFER_LIMIT);
            /* there are unprocessed jobs */
            if (!feof(file))
            {
		cant_rows++;
                fgets(buffer, BUFFER_LIMIT, file);
                // here we have unprocessed jobs , we send one job to the worker
                /* compute job */
                /* pack data of job into the buffer msg_buffer */

		// Special processing for the last lines of the file that are empty
		if ((strcmp(buffer, "\n") == 0) || (strcmp(buffer, "\0") == 0))
                {
		    printf("SALGO1 y BUFFER: %s\n", buffer);
	            // send stop msg to worker               
                    MPI_Send(buffer, BUFFER_LIMIT, MPI_CHAR, worker_rank, TAG_STOP, MPI_COMM_WORLD);
                    finished_workers++;
  	        }
                else
		{
		    /* mark worker with rank my_rank as working on a job */
		    // Envío 4 indices de sqp1
                    MPI_Send(buffer, BUFFER_LIMIT, MPI_CHAR, worker_rank, TAG_JOB_DATA, MPI_COMM_WORLD);
		    // Envío md
		    MPI_Send(md, md_size, MPI_CHAR, worker_rank, TAG_JOB_DATA, MPI_COMM_WORLD);
		    // Envío ldiv
		    MPI_Send(ldiv, ldiv_size, MPI_CHAR, worker_rank, TAG_JOB_DATA, MPI_COMM_WORLD);
		    // Envío centros orbitales
                    MPI_Send(centros, centros_size, MPI_CHAR, worker_rank, TAG_JOB_DATA, MPI_COMM_WORLD);
		}
            }
            else
            {
		printf("SALGO2 y BUFFER: %s\n", buffer);
                // send stop msg to worker
                MPI_Send(buffer, BUFFER_LIMIT, MPI_CHAR, worker_rank, TAG_STOP, MPI_COMM_WORLD);
                finished_workers++;
            }
        }
        else
        {
            // We got a result message
            MPI_Recv(buffer, BUFFER_LIMIT, MPI_CHAR, worker_rank, TAG_RESULT, MPI_COMM_WORLD, &stat2);
            /* put data from buffer into a global result */
            /* mark worker with rank worker_rank as stopped */
        }
    }
    fclose(file);

    // TODO: ciclar n workers veces esperando guardado de mats

    // Finalización y guardado de tiempos.
    octave_value_list result_af = feval ("agua_finalizacion", arguments_agua_finalizacion, 1);

    if (result_cf.length() > 0) {
        if (result_af.length() > 0) {
            arguments_save_times (1) = result_af(0); // comp_v_time
            arguments_save_times (3) = result_af(1); // energy
        }
        if (result_lcaomain.length() > 0) {
            arguments_save_times (2) = result_lcaomain(1); // lcaomain_time
        }
    }
    feval ("save_times", arguments_save_times, 1);

    printf("SQP1_ROWS: %i\n", cant_rows);
}

void worker(int rank, octave_value_list arguments_lcao, octave_value_list result_lcao)
{
    int stopped = 0;
    MPI_Status stat, stat2;
    char in_buffer[BUFFER_LIMIT] = "";
    int out_buffer[4];
    octave_value_list functionArguments;
    octave_value_list result_call_octave;

    char md[MATRIX_LIMIT];
    char centros[MATRIX_LIMIT];
    char ldiv[MATRIX_LIMIT];

    do
    {
        // Here we send a message to the master asking for a job
        MPI_Send(in_buffer, BUFFER_LIMIT, MPI_CHAR, 0, TAG_ASK_FOR_JOB, MPI_COMM_WORLD);
        MPI_Probe(0, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);

        if (stat.MPI_TAG == TAG_JOB_DATA)
        {
            // Retrieve job data from master into msg_buffer
	    // Recibe los 4 indices de sqp1
            MPI_Recv(in_buffer, BUFFER_LIMIT, MPI_CHAR, 0, TAG_JOB_DATA, MPI_COMM_WORLD, &stat2);
	    // Recibe md
            MPI_Recv(md, MATRIX_LIMIT, MPI_CHAR, 0, TAG_JOB_DATA, MPI_COMM_WORLD, &stat2);
	    // Recibe ldiv
            MPI_Recv(ldiv, MATRIX_LIMIT, MPI_CHAR, 0, TAG_JOB_DATA, MPI_COMM_WORLD, &stat2);
	    // Recibe centros orbitales
            MPI_Recv(centros, MATRIX_LIMIT, MPI_CHAR, 0, TAG_JOB_DATA, MPI_COMM_WORLD, &stat2);

	    parse_string(in_buffer, out_buffer);

	    //Invocar a octave para computar lcao.
 	    arguments_lcao (0) = md;
	    arguments_lcao (1) = centros;
	    arguments_lcao (2) = out_buffer[0];
            arguments_lcao (3) = out_buffer[1];
            arguments_lcao (4) = out_buffer[2];
            arguments_lcao (5) = out_buffer[3];

            result_lcao = feval ("lcao", arguments_lcao, 1);
	  
            // work on job_data to get a result and store the result into in_buffer
            // and send in_buffer to master
            char OK[3] = "OK";
            MPI_Send(OK, 1, MPI_CHAR, 0, TAG_RESULT, MPI_COMM_WORLD);
        }
        else
        {
            // We got a stop message we have to retrieve it by using MPI_Recv
            // But we can ignore the data from the MPI_Recv call
            MPI_Recv(in_buffer, BUFFER_LIMIT, MPI_CHAR, 0, TAG_STOP, MPI_COMM_WORLD, &stat2);
            if (stat2.MPI_TAG == TAG_STOP)
            {
		// TODO: invocar a octave para guardar mat1_myrank y mat2_myrank
		// TODO: enviar msj al master que ya guardo las mats
                stopped = 1;
            }
        }
    } while (stopped == 0);
}

int main(int argc, char **argv)
{
    const char * argvv [] = {"" /* name of program, not relevant */, "--silent"};
    
    octave_main (2, (char **) argvv, true /* embedded */);

    int rank, nproc;

    FILE *file;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nproc);

    octave_value_list arguments_config_file;
    octave_value_list result_cf = feval ("config_file", arguments_config_file, 1);
   

    if (rank == 0)
    {
        octave_value_list arguments_lcaomain;
    	arguments_lcaomain (0) = 0;
   	arguments_lcaomain (1) = nproc;
    	arguments_lcaomain (2) = argv[1]; // mdatos

	octave_value_list arguments_agua_finalizacion;
	arguments_agua_finalizacion (0) = nproc;
	arguments_agua_finalizacion (1) = 0;
	arguments_agua_finalizacion (2) = argv[1];

	octave_value_list arguments_save_times;
        arguments_save_times (0) = argv[1]; // mdatos
	arguments_save_times (4) = rank; // rank
        arguments_save_times (5) = argv[2]; // seed
        arguments_save_times (6) = argv[3]; // numbers of processors

        master(file, nproc - 1, arguments_lcaomain, arguments_agua_finalizacion, arguments_save_times, result_cf, result_lcao);
    }
    else
    {
	octave_value_list arguments_lcao;
        arguments_lcao (6) = 0;
        arguments_lcao (7) = rank;
        arguments_lcao (8) = argv[2];
        arguments_lcao (9) = argv[1];
        arguments_lcao (10) = argv[3];

        worker(rank, arguments_lcao, result_lcao);
    }

    MPI_Finalize();

    clean_up_and_exit(0,1);

    return 0;
}
