#include "comunes.h"

char *substring(size_t start, size_t stop, const char *src, char *dst, size_t size)
{
   int count = stop - start;
   if ( count >= --size )
   {
      count = size;
   }
   sprintf(dst, "%.*s", count, src + start);
   return dst;
}

/*Elimino las 4 primeras filas de texto, buscando los caracteres enter (ascii=10)*/
char * remover_encabezado_matriz(char * sqp1){
    char * aux = sqp1, *resultado;
    int i=0, count_enter=0;

    //busco los primeros 4 enters
    for(i=0; count_enter<5;i++){
        //printf("%c",aux[i]);
        if(aux[i]==10)
            count_enter++;
    }
    resultado = (char *) malloc((strlen(sqp1)-i)*sizeof(char));
    resultado = substring(i,strlen(sqp1),sqp1,resultado,strlen(sqp1)-i);
    
    return resultado;
}

int contar_filas(char * matriz){
    int i=0, count_enter=1;

    //busco los primeros 4 enters
    for(i=0; i<strlen(matriz);i++){
        //printf("%c",matriz[i]);
        if(matriz[i]==10)
            count_enter++;
    }
    return count_enter;
}

char * load_matrix(const char * md_name, long * size_read){

    int fd_md= open(md_name,O_RDONLY);
    struct stat sdata;
    long num_to_read;
	
	if (fd_md<0){
		printf("Error en la apertura del archivo %s.\n",md_name);
		exit(-1);
	}

    if (stat(md_name, &sdata) != 0){
	    printf ("Error: cannot stat file %s\n", md_name);
    	exit (-1);
    }
    num_to_read = sdata.st_size;
	
   // long num_to_read=lseek(fd_md, 0, SEEK_END);

    char * matrix = (char * ) malloc(num_to_read);
	if (matrix==0){
		printf("Error en la asignación de memoria para la matriz md\n");
		exit(-1);
	}
	lseek(fd_md,0,0);
	ssize_t num_read = read(fd_md, matrix, num_to_read);

	if (close(fd_md)<0){
		printf("Error al cerrar md.\n");
		exit(-1);
	}

	if (num_read<0 || num_read != num_to_read){
		printf("Error al leer md, o no se leyo lo suficiente. num_read %ld num_to_read %ld \n",num_read,num_to_read);
		exit(-1);
	}
    
    * size_read = num_read;
    return matrix;
}
