function [fxr]=m11_m11(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
global intqrec
puntos=24;
nmax=15;
	liz=[0:pi:(nmax-1)*pi];
	lsz=[pi:pi:nmax*pi];
	limz=[liz;lsz];
	limx=repmat([0;1],1,size(limz,2));
 	limy=limx;
 	a=a1-a2;
 	a=norm(a);
	c=b1-b2;
  c=norm(c);
  intrec=0;
  fxr=intquad3('fu11_11',limx,limy,limz,puntos,...
     a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
  fxr=(4/pi)^(2)*(alfa1*alfa2*beta1*beta2)^(2.5)*a^5*c^5*fxr;
  fxr=sum(fxr);
  return
