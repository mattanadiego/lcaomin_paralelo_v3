function [md,centros,ldiv]=magua_25(param0)
global mdatos
mdatos='magua_25';
% cambios realizados el 18/8/12
sel=[1:25];
param=[
   1.84026582344112;%1.83939995005263;%1.84417365541148;%1.9;%1.81;      %1 dist OH
   0.0647861; %2 el a
   3.318884;%3 el b
   1.3;     %4         
   54.73561;   %5 angulo en los tetraedros
   2.5;  %6
   54.73561; %7   
   0.83093576364431;%0.83060474338980;%0.83004208662760;%0.82794536775765;%0.9;%0.82909743259060;%0.82880552480745;%0.82880552480745;%0.82849165094212;%0.82815980940996;%0.82777068646675;%0.82737900321885;%0.82680502046877;%0.82635586688362;%0.82593469782704;%0.82531507900243;%0.82455853468586;% 0.82382742926509;%0.82250188127395;   %0.81982040725641;%   	0.79425742550393;%   	0.7;%8 posicion del gaussiano flotante en el segmento OH
   0.83093576364431;%1.15352941326342;%1.15243811354078;%1.15146711224393  ;%1.14169816594341;%1.13974528105902;%1.13974528105902;%1.13764267667497;%1.13537205975939;%1.13303186425539;%1.13045645780194;%1.12763075950713;%1.12466796335661;%1.12152237802559;%1.11805799578961;%1.11443063875320;%1.11059266877258;%1.10650232294715;%1.10225630154408;%1.09539243051783;%0.9;%9 tama�o del gaussiano flotante, su alfa  
   7.9013263;%10 alfa del oxigeno
   54.00012287539607;%54.00004018087790;%53.99996860694193;%54;%52.25;%11 angulo/2 entre los OH
   0.7];%12
if ~isempty(param0)
   %param(8)=param0(1); %se optimizan la posicion y el tama�o de los gs flotantes
   %param(9)=param0(2);
   param(1)=param0(1); 
   param(11)=param0(2);
end
sr5=param(2)*param(3)^5;
sr4=param(2)*param(3)^4;
sr3=param(2)*param(3)^3;
sr2=param(2)*param(3)^2;
sr1=param(2)*param(3);

epjor=0.005;
R1=epjor*0.5*((3/sr1)^0.5);
R2=epjor*0.5*((3/sr2)^0.5);
R3=epjor*0.5*((3/sr3)^0.5);
R4=epjor*0.5*((3/sr4)^0.5);
R5=epjor*0.5*((3/sr5)^0.5);

cz1=R1*cos(pi*param(5)/180);
cy1=R1*sin(pi*param(5)/180);
cz2=R2*cos(pi*param(5)/180);
cy2=R2*sin(pi*param(5)/180);
cz3=R3*cos(pi*param(5)/180);
cy3=R3*sin(pi*param(5)/180);
cz4=R4*cos(pi*param(5)/180);
cy4=R4*sin(pi*param(5)/180);
cz5=R5*cos(pi*param(5)/180);
cy5=R5*sin(pi*param(5)/180);

pz1=R1*cos(pi*param(5)/180);
px1=R1*sin(pi*param(5)/180);
pz2=R2*cos(pi*param(5)/180);
px2=R2*sin(pi*param(5)/180);
pz3=R3*cos(pi*param(5)/180);
px3=R3*sin(pi*param(5)/180);
pz4=R4*cos(pi*param(5)/180);
px4=R4*sin(pi*param(5)/180);
pz5=R5*cos(pi*param(5)/180);
px5=R5*sin(pi*param(5)/180);

cyh=param(1)*sin(pi*param(11)/180);
czh=param(1)*cos(pi*param(11)/180);

%coordenadas de los H con alfas iguales a 1 deben estar en 2 y 3
%    0       cyh	     czh     1        1;
%    0      -cyh	     czh     1        1;

% coordenadas de los gaussianos entre OH � SH "flotantes" estan en 24 y 25
%  0       cyh*param(8)  czh*param(8)  param(9)  2;
%  0      -cyh*Param(8)  czh*param(8)  param(9)  2;

md=[0         0          0   param(10)  1; %1
    0       cyh	     czh     1        1; %2
    0      -cyh	     czh     1        1; %3
    0       cy1	     cz1     sr1      2; %4
    0      -cy1	     cz1     sr1      2; %5
  px1         0	    -cz1     sr1      2; %6
 -px1         0	    -cz1     sr1      2; %7
    0       cy2	     cz2     sr2      2; %8
    0      -cy2	     cz2     sr2      2; %9
  px2         0	    -cz2     sr2      2; %10
 -px2         0	    -cz2     sr2      2; %11
    0       cy3	     cz3     sr3      2; %12
    0      -cy3	     cz3     sr3      2; %13
   px3         0	    -cz3     sr3      2; %14
  -px3         0	    -cz3     sr3      2; %15
     0       cy4	     cz4     sr4      2; %16
     0      -cy4	     cz4     sr4      2; %17
   px4         0	    -cz4     sr4      2; %18
  -px4         0	    -cz4     sr4      2; %19
    0       cy5	     cz5     sr5      2; %20
    0      -cy5	     cz5     sr5      2; %21
  px5         0	    -cz5     sr5      2; %22
  -px5         0	    -cz5     sr5      2; %23
  0       cyh*param(8)  czh*param(8)  param(9)  2; %24
  0      -cyh*param(8)  czh*param(8)  param(9)  2]; %25

%%%%%%%%%%%%%%%%%
md=md(sel,:);
ldiv=sum(md(:,5)==1);

%Definici�n de los centros
centros=[];
centros=md([1:3],1:3); %para OH2 y SH2   
%centros=md([1:1],1:3); %para atomo libre

if (isempty(centros)&& ldiv)
   centros=md(1:ldiv,1:3);
end
centros=centros';

% Valores de los z. La suma debe ser par. La longitud de zhh debe
% coincidir con la las columnas de centros. 
if ~isempty(centros)
 zhh=[8 1 1]; %OH2
 %zhh=[8]; %oxigeno libre
 %zhh=[16]; %azufre libre
 %zhh=[16 1 1]; %SH2
   centros=[centros;zhh];
end
md=md';
return
