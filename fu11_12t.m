% Funcion que se integra en la rutina m11_12t.
function [fu]=fu11_12t(terxy,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
u1=terxy(:,1);
u2=terxy(:,2);
[t,sy,sxy]=te11_12t(u1,u2,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
f0=efe0(t);
rb1b2=(b1-b2)'*(b1-b2);
d2=(beta1^2*beta2)./sy;
num=exp(-u1.^2-u2.^2);
num=num.*(u1.^3).*(u2.^3);
num=num.*exp(-d2*rb1b2);
den=sy.*sqrt(sxy);
fu=num./den;
fu=(fu.*f0)';
return

% ================================================
% Procedimiento que genera la combinacion convexa*/
function [y]=pconv(u,a1,alfa1,a2,alfa2)
y=zeros(size(u,1),3);
y(:,1)=alfa1^2*a1(1)+4*alfa2*u.^2*a2(1);
y(:,2)=alfa1^2*a1(2)+4*alfa2*u.^2*a2(2);
y(:,3)=alfa1^2*a1(3)+4*alfa2*u.^2*a2(3);
yd=alfa1^2+4*u.^2*alfa2;
y=y./(yd*ones(1,3));
return
     
% ================================================
% Procedimiento que genera el t de la funcion f0*/
function [t,ty,txy]=te11_12t(u1,u2,a1,alfa1,...
   a2,alfa2,b1,beta1,b2,beta2)
ty=beta1^2+4*u2.^2*beta2;
txy=(alfa1+alfa2)^2*u2.^2+beta1^2*u1.^2+4*beta2*(u1.^2).*(u2.^2);
t=0.25*(alfa1+alfa2)^2*ty./txy;
y2=pconv(u2,b1,beta1,b2,beta2);
yy=ones(size(y2,1),1)*a1'-y2;
norma=sum((yy.*yy)')';
t=t.*norma;
return

% Procedimiento que genera el t de la funcion f0*/
function [t,ty,txy]=te11_12t1(u1,u2,a1,alfa1,...
   b1,beta1,b2,beta2)
ty=beta1^2+4*u2.^2*beta2;
txy=4*alfa1^2*u2.^2+beta1^2*u1.^2+4*beta2*(u1.^2).*(u2.^2);
t=alfa1^2*ty./txy;
y2=pconv(u2,b1,beta1,b2,beta2);
yy=ones(size(y2,1),1)*a1'-y2;
norma=sum((yy.*yy)')';
t=t.*norma;
return