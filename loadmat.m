function [value]=loadmat(matName)
 fid   = fopen(matName,"r+");
 ltype = fread(fid,1,'int8'); %leemos el primer byte (tipo de dato)
 lrow  = fread(fid,1,'int32'); %leemos cantidad de filas
 lcol  = fread(fid,1,'int32'); %leemos cantidad de columnas

 %ltype = 0=> entero16, 1=> entero32, 2=> real, 3=>doubble, 
 switch ltype
    case 0
        value = fread(fid,lrow*lcol,'int16');
    case 1
        value = fread(fid,lrow*lcol,'int32');
    case 2
        value = fread(fid,lrow*lcol,'float32');
    case 3 
        value = fread(fid,lrow*lcol,'float64');
 endswitch

% disp(ltype)
% disp(lrow)
% disp(lcol)
% disp(value)
 fclose(fid);
 if lrow>1 || lcol>1
     value =  reshape(value, lrow, lcol);
 endif
 
return 
