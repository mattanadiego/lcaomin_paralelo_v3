function [md,lcaomain_time] = lcaomain(my_rank, nproc, mdatos, param0, gcnt)

disp('Comienza lcaomain !!!')
lcaomain_time_tic = tic;

global lcaomain_path %param_c_size param0 gcnt

clc

lcaomin_path = getenv("LCAOMINPATH");
ejemplospath = strcat(lcaomin_path, 'agua/');
addpath(lcaomin_path, ejemplospath);
% limpia
param0 = [];
gcnt = [];

global mum

% *************************************************************
%      ENCABEZAMIENTO Y VARIABLES GLOBALES. NO MODIFICAR.
% *************************************************************

global orden puntosx puntosr
orden = 0;
puntosx = 20;
puntosr = 20;
globales

% *************************************************************
% 	      DATOS A MODIFICAR CON CADA EJEMPLO
% *************************************************************
% Identificación del ejemplo
nomarch = strcat('a',mdatos);
optim = 1;
disp('antes de feval')
%Valores de los z. La suma debe ser par!!
[md,centros,ldiv] = feval(mdatos,param0);

clc
save centros centros

%================ nueva modificacion 20/10 ====================

% Variables globales.
global nomarch %lcaomain_path mdatos 
   
% Limites
clc
clsmd = size(md, 2);
md = get_dat(md);
sz = 0.5 * sum(centros(4,:)); %Semisuma de zhh

sqp1 = sqp(clsmd); 

disp('antes de index_par_dos')
index_par_dos = index_par_dos(sqp1);

%==============================================================

save index_par_dos index_par_dos
savemat('md', 3, md); 
savemat('ldiv', 0, ldiv);
sqp1 = INDICES2(clsmd);
save sqp1 sqp1
disp('antes de PARTIR SQP')
partirSqp1(sqp1,nproc);

lcaomain_time = toc(lcaomain_time_tic);
save lcaomain_time lcaomain_time
disp('Finaliza lcaomain')

return
