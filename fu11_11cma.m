function [ut,vt,f0,Dfu,Dfv,Dfu2,Dfv2,Dfuv2,uu0,vv0]=fu11_11cma(parxr,pesow,xcr,pm,u0,v0,a1,alfa1,a2,alfa2,a3,alfa3,a4,alfa4,orden)
a12=a1-a2;
a34=a3-a4;
a12=norm(a12);
a34=norm(a34);
f0=(sin(pm.*xcr)./(pm.*xcr))';
u=parxr(:,1);
ptu=(u.*(1-u));
uu0=(u-u0);
vv0=(u-v0);
u3=parxr(:,2);
ptv=ptu.*(u3.^2);
z1=alfa2^2+(alfa1^2-alfa2^2)*u+ptv;
z1=sqrt(z1)*a12;
z2=alfa4^2+(alfa3^2-alfa4^2)*u+ptv;
z2=sqrt(z2)*a34;
ut=ptu.*k1(z1).*pesow;
vt=ptu.*k1(z2).*pesow;
sf0=[];
cf0=[];
Dfu=[];
Dfv=[];
Dfu2=[];
Dfv2=[];
Dfuv2=[];

if orden >=2
   % Calculo de las derivadas
   sf0=(1/pm^2)*f0;
   cf0=(1/pm^2)*cos(pm.*xcr)';
   [p1,pa,pc]=p(u0,v0,a1,a2,a3,a4);
   Dfu=pa*(cf0-sf0);
   Dfv=pc*(sf0-cf0);
end
if orden >=3	
   % c�lculo de las derivadas de segundo orden
   ab=a2-a1;
   cd=a4-a3;
   abcd=ab'*cd;
   
   csenou= (3*pa^2./pm^4)-(a12^2./pm^2)-(xcr.^2.*pa^2/pm^2);
   ccosu= (a12^2./pm^2)-(3*pa.^2./pm^4);
   Dfu2=ccosu * cos(pm.*xcr)';
   Dfu2a2=csenou' .*f0;
   Dfu2=Dfu2+Dfu2a2;
  
   csenov= (3*pc^2./pm^4)-(a34^2./pm^2)-(xcr.^2.*pc^2/pm^2);
   ccosv= (a34^2./pm^2)-(3*pc.^2./pm^4);
   Dfv2=ccosv * cos(pm.*xcr)';
   Dfv2a2=csenov' .*f0;
   Dfv2=Dfv2+Dfv2a2;

   csenouv= (-3*pc*pa./pm^4)+(abcd./pm^2)+(xcr.^2.*pc*pa/pm^2);
   ccosuv= (-abcd./pm^2)+(3*pc*pa./pm^4);
   Dfuv2=ccosuv * cos(pm.*xcr)';
   Dfuv2a2=csenouv' .*f0;
   Dfuv2=Dfuv2+Dfuv2a2;

end
return

% -----------------------------------------------
function [k11]=k1(q)
	k11=q.^(-5)*(pi/2)^(0.5).*exp(-q);
	k11=k11.*(3+3*q+q.^2);
return

% -----------------------------------------------
function [p1,pa,pc]=p(u,v,a1,a2,a3,a4)	
save uu u v
	px=(u*a2(1)+(1-u)*a1(1))-(v*a4(1)+...
      (1-v)*a3(1))*ones(1,size(u,2));
  py=(u*a2(2)+(1-u)*a1(2))-(v*a4(2)+...
     (1-v)*a3(2))*ones(1,size(u,2));
  pz=(u*a2(3)+(1-u)*a1(3))-(v*a4(3)+...
     (1-v)*a3(3))*ones(1,size(u,2));
  p1=sqrt((px.*px+py.*py+pz.*pz));
  pa=[px py pz]*(a2-a1);
  pc=[px py pz]*(a4-a3);
return

