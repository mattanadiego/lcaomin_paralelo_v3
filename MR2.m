function [x,f,grad,iter,ff]=MR(fun,x0)
global epsil delta tol 
x=x0;
grad=ones(length(x0),1);
grad0=zeros(length(x0),1);
iter=1;
ff=[];
lam=1
while (norm(grad)>tol)&(iter<200)
   y=grad-grad0;
   alf=-(grad'*y)/(lam*grad'*grad);
   if (alf<=epsil)|(alf>=(1/epsil))
      alf=delta;
   end
   lam=1/alf;
   x=x-lam*grad;
   %x=x-alf*grad;
   f=feval(fun,x);
   ff=[ff;f];
   grad0=grad;
   grad=gradiente(fun,f,x);
      %alf=-(grad'*y)/(lam*grad'*grad);
   iter=iter+1;
end
return

