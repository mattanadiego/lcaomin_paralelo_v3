% Funcion integrada en cv_4, utilizada en calc_va

function [yyr,yyi]=fcv_4(x,alfa,beta,norma2)
	yyr=exp(-(alfa+beta)*norma2*(x.^2));
   yyi=[];
return   
