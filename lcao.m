function [comp_4in_time]=lcao(md,centros,f_a1,f_a2,f_b1,f_b2,optim,my_rank,num_seed,mdatos,num_procs)

% *************************************************************
%	    RUTINAS DE CALCULO. NO MODIFICAR!!
% *************************************************************

data_path = getenv("LCAOMINPATH");
data_path = strcat(data_path, 'agua/');

time_file = strcat(data_path, mdatos);
time_file = strcat(time_file, '_');
procs_s = num2str(num_procs);
time_file = strcat(time_file, procs_s);
time_file = strcat(time_file, 'procs_time_proc');
rank_s = num2str(my_rank);
time_file = strcat(time_file, rank_s);

%disp('Comienza comp_4in')
comp_4in_time_tic = tic;
[vclmn,mat1,mat2,indices] = comp_4in(my_rank,f_a1,f_a2,f_b1,f_b2);
comp_4in_time = toc(comp_4in_time_tic);
save(time_file, 'comp_4in_time','-append');
%disp('Finaliza comp_4in');

return
