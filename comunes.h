#ifndef COMUNES_H_
#define COMUNES_H_

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

char *substring(size_t start, size_t stop, const char *src, char *dst, size_t size);

/*Elimino las 4 primeras filas de texto, buscando los caracteres enter (ascii=10)*/
char * remover_encabezado_matriz(char * sqp1);

int contar_filas(char * matriz);

char * load_matrix(const char * md_name, long * size_read);

#endif
