% ================================================
function [cpar]=ctrlpar(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
if (par1(2)<=ldiv)&&(par2(2)<=ldiv)
   cpar=cuatro_m1(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
elseif (par1(1)>ldiv)&&(par2(1)>ldiv)
   % 4 de m�dulo 2
   cpar='22_22  ';
else
   % 3 o menos de m�dulo 1
   cpar=tres_m1(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
end
return


% ================================================
function[cpar]=cuatro_m1(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
% 4 de modulo 1
if (~any(a1-a2)) && (~any(b1-b2))
    cpar='1b_1b  ';			%(1sa 1sa'|1sa" 1sa"') ro=0, tau=0 o tau<>0
 									%(1sa 1sa'|1sb 1sb')   ro=0, tau=0 o tau<>0)
elseif (~any(a1-a2))||(~any(b1-b2))
   if ~any(a1-a2)	
      cpar='11_11t1';		%(1sa 1sa|1sb 1sc) (1sa 1sa|1sa' 1sb) 
   elseif ~any(b1-b2)
         cpar='11_11t2';	%(1sb 1sc|1sa 1sa) (1sa 1sb'|1sb 1sb) 
   end
else
   cpar='11_11  ';			%(1sa 1sb|1sc 1sd)  !Siempre a<>b y c<>d
   								%(1sa 1sb|1sb 1sc)  !Siempre a<>b y b<>c
                           %(1sa 1sb|1sa 1sc)  !Siempre a<>b y a<>c
                           %(1sa 1sb|1sc 1sb)  !Siempre a<>b y c<>b
                           %(1sa 1sb|1sa' 1sb')!Siempre a<>b
end
return   
   


% ================================================
function[cpar]=tres_m1(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
% 3 de m�dulo 1
if (par1(2)<=ldiv)&&(par2(1)<=ldiv)
   cpar='11_12c1';
elseif (par1(1)<=ldiv)&&(par2(2)<=ldiv)
   cpar='11_12c2';
end
% 2 de m�dulo 1
if (par1(2)<=ldiv)&&(par2(1)>ldiv)
   cpar='11_22  ';
end
if (par1(1)<=ldiv)&&(par1(2)>ldiv)&&(par2(2)>ldiv)
   if par2(1)<=ldiv
      cpar='12_12  ';
   else
      % 1 de m�dulo 1
      cpar='12_22  ';
   end
end
return


function[cpar]=cuatro_m1old(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
% 4 de modulo 1
if (par1(1)==par1(2))&&(par2(1)==par2(2))
   if par1(2)==par2(1)		
      cpar='41     ';		%(1sa 1sa|1sa 1sa)
   else
      cpar='1b_1b  ';		%(1sa 1sa|1sb 1sb) (1sa 1sa|1sa' 1sa')
   end
else
   if (par1(1)==par1(2))||(par2(1)==par2(2))
      if par1(1)==par1(2)	%(1sa 1sa|1sb 1sc) (1sa 1sa|1sa' 1sb)
         cpar='11_11t1';
      elseif par2(1)==par2(2)
         cpar='11_11t2';	%(1sb 1sc|1sa 1sa) (1sa 1sb'|1sb 1sb)
      end
   else
      if (par1(1)==par2(1))&&(par1(2)==par2(2))
         cpar= '11_11b ';	%(1sa 1sb|1sa 1sb) (1sa 1sb'|1sa 1sb')
      else
         cpar='11_11  ';	%(1sa 1sb|1sc 1sd)
      end
   end
end
return   

function[cpar]=tres_m1old(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
% 3 de m�dulo 1
if (par1(2)<=ldiv)&&(par2(1)<=ldiv)
   if(~any(a1-a2))
      cpar='11_12t1';
   else
      cpar='11_12c1';
   end
elseif (par1(1)<=ldiv)&&(par2(2)<=ldiv)
   if(~any(b1-b2))
      cpar='11_12t2';
   else
      cpar='11_12c2';
   end
end
% 2 de m�dulo 1
if (par1(2)<=ldiv)&&(par2(1)>ldiv)
   cpar='11_22  ';
end
if (par1(1)<=ldiv)&&(par1(2)>ldiv)&(par2(2)>ldiv)
   if par2(1)<=ldiv
      cpar='12_12  ';
   else
      % 1 de m�dulo 1
      cpar='12_22  ';
   end
end
return
