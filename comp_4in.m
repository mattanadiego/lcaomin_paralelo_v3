function [vsf,mat1,mat2,vindm]=comp_4in(my_rank,f_a1,f_a2,f_b1,f_b2)
global orden mat1 mat2 vindm rank vsf

%WORKER = my_rank
if nargin==3
   vindm=[];
end

if isempty(orden)
   orden = 0;
end

md = loadmat('md');
ldiv = loadmat('ldiv');
sqp1 = [f_a1,f_a2,f_b1,f_b2];
%SQP = sqp1
%MAT1 = mat1

clsmd=size(md,2); %cantidad de columnas
clsmd2=clsmd^2;

sqp1=size(sqp1,1);

name_mat1 = strcat('mat1_',num2str(my_rank));
name_mat2 = strcat('mat2_',num2str(my_rank));
name_vsf = strcat('vsf_',num2str(my_rank));

[info,err,msg]=stat(name_mat1);
%if err>=0
%   mat1=loadmat(name_mat1);
%   mat2=loadmat(name_mat2);
%   vsf=loadmat(name_vsf);
%else
%   mat1=zeros(clsmd2^2,1);
%   mat2=zeros(clsmd2^2,1);
%   vsf=[];
%end

globales
   a1=md(1:3,f_a1);
   alfa1=md(4,f_a1);
   
   a2=md(1:3,f_a2);
   alfa2=md(4,f_a2);
      
   b1=md(1:3,f_b1);
   beta1=md(4,f_b1);
     
   b2=md(1:3,f_b2);
   beta2=md(4,f_b2);
   
   par1=[f_a1,f_a2];
   par2=[f_b1,f_b2];

   cpar=ctrlpar(par1,par2,ldiv,a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
   fxr=calc_int(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,cpar,orden);

   vsf=[vsf;fxr];
   [mat1,mat2]=llenamat(fxr,mat1,mat2,f_a1,f_a2,f_b1,f_b2,clsmd,sqp1);

%savemat(name_mat1,3,mat1);
%savemat(name_mat2,3,mat2);
%savemat(name_vsf,3,vsf);

vindm = sqp1;

return

% ================================================

function [fxr]=calc_int(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,cpar,orden)
switch cpar
case '1b_1b  '
   fxr=m1b_m1b(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '11_11t1'
   fxr=m11_m11t(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '11_11t2'
   fxr=m11_m11t(b1,beta1,b2,beta2,a1,alfa1,a2,alfa2);
case '11_11  '
   switch orden
   case 0
      fxr=m11_m11c(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
   otherwise
      fxr=m11_m11cma(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2,orden);
   end
case '11_12c1'
   fxr=m11_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '11_12c2'
   fxr=m11_m12(b1,beta1,b2,beta2,a1,alfa1,a2,alfa2); 
case '11_22  '
   fxr=m11_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '12_12  '
   fxr=m12_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '12_22  '
   fxr=m12_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case '22_22  '
   fxr=m22_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
case 'clmn   '
   fxr=fourind(a1,alfa1,a2,alfa2,...
      b1,beta1,b2,beta2,par1,par2,sqp1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [mat1,mat2]=llenamat(fxr,mat1,mat2,...
   mu0,nu0,ro0,sg0,clsmd,sqp1)

mu=mu0;
nu=nu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=mu0;
nu=nu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

% Cambiamos 
aux1=mu0;
aux2=nu0;
mu0=ro0;		%sqp1(par2,1);
nu0=sg0;		%sqp1(par2,2);
ro0=aux1;		%sqp1(par1,1);
sg0=aux2;		%sqp1(par1,2);

mu=mu0;
nu=nu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=mu0;
nu=nu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=ro0;
sg=sg0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;      
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

mu=nu0;
nu=mu0;
ro=sg0;
sg=ro0;
ind=((ro-1)*clsmd+sg)*clsmd^2+((mu-1)*clsmd+nu)-clsmd^2;
mat1(ind,1)=fxr;   
ind2=((ro-1)*clsmd+mu)*clsmd^2+((sg-1)*clsmd+nu)-clsmd^2;
mat2(ind2,1)=fxr;      

return
