% Esta funci�n expande un vector v en una matriz sim�trica ms

function [ms]=xpnd(v)
k=length(v);
m=(sqrt(1+8*k)-1)/2;
ms=repmat(0,m,m);
for i=1:m
   ls=i*(i+1)/2;
   li=ls-i+1;
   ms(i,1:i)=v(li:ls)';
end
ms=ms+ms'-diag(diag(ms));
return