% Calculo de cantidades de 4 indices(mod1-mod1|mod1-mod2)
function [fxr]=m11_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
if(~any(a1-a2)) %Tricentricas
   puntos = 40;
   cte=(alfa1*alfa2*beta1/pi)^(1.5)*(2*beta2/pi)^(.75);
   cte=cte*(pi^1.5)*4^4/(alfa1+alfa2)^2;
   d=beta2^.75;
   num=(alfa1+alfa2)*(beta1)^1.5*(pi/2)^0.25;
   num=10^8*num/d;
   lim=1;
   while (exp(lim.^2)/(lim.^2+1))<=num
      lim=lim+1;
   end
   fxr=intdobleg('fu11_12t',[0;lim],puntos,a1,alfa1,...
      a2,alfa2,b1,beta1,b2,beta2);
   fxr=cte*fxr;
else						%Cuatricentricas
   puntos=24;
   den=sqrt(alfa1)*beta2^.75*(pi)^.25;
   num=(alfa2*beta1)^(1.5)*2^(2.75)/den;
   lim=1;
   while exp(lim.^2)/(lim.^2+1)<=10^8*num
      lim=lim+1;
   end
   [fxr]=inttripleg('fu11_12',[0;lim],puntos,...
      a1,alfa1,a2,alfa2,b1,beta1,b2,beta2);
   cte=(alfa1*alfa2*beta1)^(1.5)*(beta2)^(.75)*2^(9.75);
   cte=cte/(pi)^1.25;
   fxr=cte*fxr;
end
return

