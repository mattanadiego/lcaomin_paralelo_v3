function [x,f,grad,iter,ff]=MR(fun,x0)
global epsil delta gama sigma tol
x=x0;
f=feval(fun,x);
grad=gradiente(fun,f,x0);

den=grad'*grad;
alf=1;
iter=1;
ff=[];
f0=f;
while (norm(grad)>tol)&(iter<200)
   if (alf<=epsil)|(alf>=(1/epsil))
      alf=delta;
   end
   ff=[ff;f];
   lam=1/alf;
   x=x0-lam*grad;
   f=feval(fun,x);
   while f>f0-gama*lam*den
      lam=sigma*lam;
      x=x0-lam*grad;
      f=feval(fun,x);
   end
   x0=x;
   grad0=grad;
   f0=f;
   grad=gradiente(fun,f,x);
   y=grad-grad0;
   den=grad'*grad;
   alf=-(grad'*y)/(lam*den);
   iter=iter+1;
end
return

