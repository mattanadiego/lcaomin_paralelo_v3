function [ctrlcp1]=control2(sqp1,par1,ldiv,a1,b1)
if (sqp1(par1,2) <= ldiv)
   if ~any(a1-b1)
      ctrlcp1 = 1; 			% Modulo 1 centradas en el mismo punto 
      							% (1sa|1sa) (1sa|1sa')
   else
      ctrlcp1 = 2; 			% Modulo 1 centradas en diferentes puntos
      % (1sa|1sb) 
   end
elseif (sqp1(par1,1) > ldiv)
   ctrlcp1 = 4; 				% Modulo 2 vs. M�dulo 2
							      % (1ga|1ga') (1ga|1gb)
else
   ctrlcp1 = 3; 				% Modulo 1 vs. Modulo 2 (1sa|1ga) o (1sa|1gb)
end


function [ctrlcp1]=control2old(sqp1,par1,ldiv,a1,b1)
	if (sqp1(par1,1) == sqp1(par1,2)) && (sqp1(par1,2) <= ldiv)
      ctrlcp1 = 1; % Modulo 1 iguales con igual centro (1sa|1sa)
   elseif (sqp1(par1,1) == sqp1(par1,2)) && (sqp1(par1,2) > ldiv)
      ctrlcp1 = 4; % Modulo 2 iguales con igual centro (1ga|1ga)
   elseif (sqp1(par1,1) < sqp1(par1,2)) && (sqp1(par1,2) <= ldiv)
      if any(a1-b1) % Modulo 1 diferentes centros (1sa|1sb)
         ctrlcp1 = 2;
      else
         ctrlcp1 = 6; % Modulo 1 diferentes con igual centro (1sa|1sa')
      end
   elseif (sqp1(par1,1) < sqp1(par1,2)) && (sqp1(par1,1) > ldiv)
      ctrlcp1 = 5; % Modulo 2 diferentes centros (1ga|1gb)
   else
      ctrlcp1 = 3; % Modulo 1 vs. Modulo 2 entros (1sa|1gb) o (1sa|1ga)
   end
return
         
function [ctrlcp1]=control2new(sqp1,par1,ldiv,a1,b1)
if (sqp1(par1,2) <= ldiv)
   if ~any(a1-b1)
      ctrlcp1 = 1; 			% Modulo 1 centradas en el mismo punto 
      							% (1sa|1sa) (1sa|1sa')
   else
      ctrlcp1 = 2; 			% Modulo 1 centradas en diferentes puntos
      % (1sa||1sb) 
   end
elseif (sqp1(par1,2) > ldiv)
   ctrlcp1 = 4; 				% Modulo 2 vs. M�dulo 2
							      % (1ga|1ga') (1ga|1gb)
else
   ctrlcp1 = 3; 				% Modulo 1 vs. Modulo 2 (1sa|1ga) o (1sa|1gb)
end
