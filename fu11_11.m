function [fu]=fu11_11(u1,u2,u3,a1,alfa1,a2,alfa2,a3,alfa3,a4,alfa4)
	a12=a1-a2;
 	a34=a3-a4;
 	a12=norm(a12);
  	a34=norm(a34);
  	pp=p(u1,u2,a1,a2,a3,a4);
     if all(pp==0)
        fu=zeros(size(u1));
        return
     end
     
 	z1=(alfa2^2+(alfa1^2-alfa2^2)*u1)+...
       (u1.*(1-u1)).*...
       ((u3./p(u1,u2,a1,a2,a3,a4)).*(u3./p(u1,u2,a1,a2,a3,a4)));
	z1=sqrt(z1)*a12;
 	z2=alfa4^2+(alfa3^2-alfa4^2)*u2;
 	z2=(z2*ones(1,size(u3,2)))+...
     ((u2.*(1-u2))*ones(1,size(u3,2))).*...
     ((u3./p(u1,u2,a1,a2,a3,a4)).*(u3./p(u1,u2,a1,a2,a3,a4)));
	z2=sqrt(z2)*a34;
  	fu=(u1.*(1-u1)).*((u2.*(1-u2))*ones(1,size(u1,2))).*...
     (sin(u3)./u3).*k1(z1).*k1(z2);
 	fu=fu./(p(u1,u2,a1,a2,a3,a4));
return;

% -----------------------------------------------
function [k11]=k1(q)
	k11=q.^(-5)*(pi/2)^(0.5).*exp(-q);
	k11=k11.*(3+3*q+q.^2);
return

% -----------------------------------------------
function [p1]=p(u,v,a1,a2,a3,a4)	
	px=(u*a2(1)+(1-u)*a1(1))-(v*a4(1)+...
      (1-v)*a3(1))*ones(1,size(u,2));
  py=(u*a2(2)+(1-u)*a1(2))-(v*a4(2)+...
     (1-v)*a3(2))*ones(1,size(u,2));
  pz=(u*a2(3)+(1-u)*a1(3))-(v*a4(3)+...
     (1-v)*a3(3))*ones(1,size(u,2));
  p1=sqrt((px.*px+py.*py+pz.*pz));
return

