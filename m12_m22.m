% Calculo de cantidades de 4 indices(mod1-mod2|mod2-mod2)
function [fxr]=m12_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
puntos = 40;
%den=(beta1+beta2)^.5*(pi)^.75*(alfa2)^.25;
den=(beta1+beta2)*(pi)^.75*(alfa2)^.25;
cte=(b1-b2)'*(b1-b2);
cte=cte*beta1*beta2/(beta1+beta2);
%lim=exp(-cte);
lim=1;
lim=(2)^4.25*(alfa1)^.5*(beta1*beta2)^.75*lim/den;
if lim<1
   lim=1;
end
lim=8*log(10)+log(lim);
lim=sqrt(lim);
fxr=intquad('fu12_22',0,lim,puntos,a1,alfa1,a2,alfa2,...
   b1,beta1,b2,beta2);
c1222=(alfa1)^(1.5)*(alfa2*beta1*beta2)^(.75)*2^(7.25)/(pi)^(.75);
c1222=c1222/(beta1+beta2);
cte=exp(-cte)*c1222;
fxr=cte*fxr;
return   
