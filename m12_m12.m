% Calculo de cantidades de 4 indices(mod1-mod2|mod1-mod2)
function [fxr]=m12_m12(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
puntos = 20;
cte=(alfa1*beta1)^(1.5)*(alfa2*beta2)^(.75)*2^(9.5);
cte=cte/pi;
d=2^7*alfa2*beta2*(alfa2+beta2)^.5;
k=cte*pi/d;
if k<1
   k=1;
end
lim=8*log(10)+log(k);
lim=sqrt(lim);
fxr=intdobleg('fu12_12',[0;lim],puntos,a1,alfa1,a2,alfa2,...
   b1,beta1,b2,beta2);
fxr=cte*fxr;
return
