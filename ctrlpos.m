function [xt,lambda]=ctrlpos(lambda,xc,d,indp)
xt=xc+lambda*d';
while ~all(xt(indp)>0)
   lambda=0.5*lambda;
   xt=xc+lambda*d';
end

while ~all(xt<=1.50*xc)
   lambda=0.1*lambda;
   xt=xc+lambda*d';
end

return

