% Calculo de cantidades de 4 indices(mod1-mod1|mod2-mod2)
function [fxr]=m11_m22(a1,alfa1,a2,alfa2,b1,beta1,b2,beta2)
nn=(b1-b2)'*(b1-b2);
ee=exp(-((beta1*beta2)/(beta1+beta2))*nn);
if (~any(a1-a2))
   puntos=60; 
   nn=(b1-b2)'*(b1-b2);
   ee=exp(-((beta1*beta2)/(beta1+beta2))*nn);
   cte=(alfa1*alfa2)^(1.5)*(beta1*beta2)^(.75)*2^(6.5)*ee;
   cte=cte/(sqrt(pi)*(alfa1+alfa2)^2*(beta1+beta2));
   denom=2^4*alfa1*alfa2*sqrt(beta1+beta2);
   num=cte/denom;
   % C�lculo del limite de integracion
   % con precision de 8 cifras lim=1;
   lim=1;
   while exp(lim.^2)/(lim.^2+1)<=10^8*num
      lim=lim+1;
   end
   fxr=intquad('fu11_22t',0,lim,puntos,a1,alfa1,a2,alfa2,...
      b1,beta1,b2,beta2);
else
   puntos=40; 
   cte=(alfa1*alfa2)^(1.5)*(beta1*beta2)^(.75)*2^(7.5)*ee;
   cte=cte/(pi*(beta1+beta2));
   denom=2^4*alfa1*alfa2*sqrt(beta1+beta2);
   num=cte/denom;
   % C�lculo del limite de integracion
   % con precision de 8 cifras lim=1;
   lim=1;
   while exp(lim.^2)/(lim.^2+1)<=10^8*num
      lim=lim+1;
   end
   fxr=intdobleg('fu11_22',[0;lim],puntos,a1,alfa1,a2,alfa2,...
      b1,beta1,b2,beta2);
end
fxr=cte*fxr;
return

   
