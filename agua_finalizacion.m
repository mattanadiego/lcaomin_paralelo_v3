function [comp_v_time,mol_total_energy,autoval,md,cs,pse,s,ctr]=agua_finalizacion(nproc,my_rank,mdatos,param0,gcnt)

%disp('Finalizo comp_4in')
%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Vuelvo a ejecutar feval para obtener centros.
%mdatos='magua';
param0=[];
[md,centros,ldiv]=feval(mdatos,param0); 

%construyo sqp1 nuevamente
clsmd=size(md,2);
md = get_dat(md);
sz=0.5*sum(centros(4,:)); %Semisuma de zhh
sqp1=sqp(clsmd); 

disp('Comienza comp_v')
comp_v_time_tic = tic;
[vt,vs,vv]=comp_v(md,centros,ldiv,sqp1);
comp_v_time = toc(comp_v_time_tic);
%save comp_v_time comp_v_time;
disp('Finalizo comp_v')

vname=strcat('V',mdatos);
save(vname,'vt','vs','vv','sqp1','sz','ldiv') 
contr_v(vname,vt,vs,vv);
matname=strcat('MAT',mdatos);

%genero mat1 y mat2 a partir de lo computado por cada procesador
[mat1,mat2,vclmn] = combinar_matrices(nproc);

save(matname,'vclmn','mat1','mat2','md','centros')
contr_mat(matname,vclmn,mat1,mat2);

[energy,autoval,cs,pse,ctr,s]=metodo(vt,vv,...
	vs,mat1,mat2,sz,sqp1,clsmd);


fflush(stdout);
save mat_autoval autoval
save mat_md md
save mat_cs cs
save mat_pse pse
save mat_s s

save jorper autoval md cs pse s
format long;
disp('Energía: ');
disp(energy)
disp('Energía total: ')
agenergia=ag_ener(centros);
save total_energy energy;
disp(energy+agenergia);
mol_total_energy = energy;
energy=energy+agenergia;
return
