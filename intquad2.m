%** INTQUAD2
%**
%** Purpose:    Integrates a specified function using Gauss-Legendre quadrature.
%**             A suite of upper and lower bounds may be calculated
%**             in one procedure call.
%**
%** Format:     y = INTQUAD2(&f,xl,yl);
%**
%** Input:      &f   pointer to the procedure containing the
%**                  function to be integrated.
%**
%**           xl     2xN vector, the limits of x.
%**
%**           yl     2xN vector, the limits of y.
%**
%**                  For xl and  yl, the first row is the upper limit and
%**                  the second row is the lower limit.  N integrations are
%**                  computed.
%**
%**       _intord    scalar, the order of the integration
%**                       2, 3, 4, 6, 8, 12, 16, 20, 24, 32, 40.
%**
%**       _intrec    scalar.  This determines the recursion level.  Users
%**                  should explicitly set this to 0 in their command files
%**
%** Output:     y    Nx1 vector of the estimated integral(s) of f(x,y)
%**                  evaluated between between the limits given by xl and yl.
%**
%**
%** Remarks:
%** User defined function f must return a vector of function values.
%** INTQUAD2 will pass to  user defined functions a vector or matrix for
%** x and y and expect a vector or matrix to be returned. Use the ".*"
%** and "./" instead of "*" and "/".
%**
%** INTQUAD2 will expand scalars to the appropriate size.  This means that
%** functions can be defined to return a scalar constant.  If users write
%** their functions incorrectly (using "*" instead of ".*", for example),
%** INTQUAD2 will not compute the expected integral, but the integral of
%** a constant function.
%**
%** To integrate over a region which is bounded by functions, rather than
%** just scalars, use INTGRAT2.
%**
%**
%** Example:    proc f(x);
%**                retp( sin(x+y) );
%**             endp;
%**
%**             let xl = { 1,
%**                        0 };
%**             let yl = { 1,
%**                        0 };
%**
%**             _intord = 12;
%**             _intrec = 0;
%**             y = intquad2(&f,xl,yl);
%**
%**             This will integrate the function sin(x+y) between
%**             x = 0 and 1, and y = 0 to 1.
%**


% Procedimiento para realizar la integración doble .
function [fx]=intquad2(F,xl,yl,puntos,varargin)
global intq2 intq3 intq6 intq8 intq12 intq16 intq20 intq24
global intq32 intq40 intq60 intq80 intq100 intq120 intrec
switch puntos 
case 2
   intq=intq2;
case 3
   intq=intq3;
case 4
   intq=intq4;
case 6
   intq=intq6;
case 8
   intq=intq8;
case 12
   intq=intq12;
case 16
   intq=intq16;
case 20
   intq=intq20;
case 24
   intq=intq24;
case 32
   intq=intq32;
case 40
   intq=intq40;
case 60
   intq=intq60;
case 80
   intq=intq80;
case 100
   intq=intq100;
case 120
   intq=intq120;
otherwise
   intq=intq12;
end

% check for complex input */
if ~isreal(xl)
   %errorlog "ERROR: Not implemented for complex matrices.";
else
   xl = real(xl);
end

if ~isreal(yl)
   %errorlog "ERROR: Not implemented for complex matrices.";
else
   yl = real(yl);
end

if intrec ==0
   if size(xl,1) ~= 2
      %errorlog "ERROR:  X limit vector must have 2 rows";
   elseif size(yl,1) ~= 2
      %errorlog "ERROR:  X limit vector must have 2 rows";
	else
      chk = [size(xl,2);size(yl,2)];
      n = max(chk);
      if ~(all(chk == 1) | all(chk == n)) 
         %errorlog "ERROR:  Limit matrices are not conformable.\r\l";
      else
         xl = zeros(2,n) + xl;
         yl = zeros(2,n) + yl;
      end
   end
end
e = intq(:,1);
w = intq(:,2);
if ~intrec
   intrec=1;
   diff = xl(2,:)' - xl(1,:)';
   xc = 0.5*( (xl(1,:)'+xl(2,:)')*ones(1,length(e))+(diff * e'));
   fx = intquad2(F,xc,yl,puntos,varargin{:});
   fx = ((diff/2).* (fx*w));
   intrec = 0;
   return
else
   xc = xl; 
   diff = yl(2,:)' - yl(1,:)';
   yc = 0.5*( (yl(1,:)'+yl(2,:)')*ones(1,length(e))+(diff * e'));
   fx = xc;
   for ii=1:size(xc,2)
      t = zeros(size(yc,1),size(yc,2));
      tt=feval(F,xc(:,ii),yc,varargin{:});
      t=t+tt;
      fx(:,ii) = ((diff/2).*(t*w));
   end
   return
end
return

