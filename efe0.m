% ================================================
% 	Procedimiento que se usa en las rutinas m11_12,
%	m11_22.
function [yy]=efe0(t)
   if all(t)
      i0=0;
      y0=0;
   else
      i0=find(t==0);
      y0=ones(length(i0),1);
   end
   i1=find(t);
   if isempty(i1)
      i1=0;
      y1=0;
   else
      t1=t(i1);
      y1=(0.5)*(pi./t1).^0.5  .* erf(t1.^0.5);
   end
   yy=[i0 y0;i1 y1];
   yy=sortc(yy,1);
   yy=yy(:,2);
   if ~yy(1)
      yy=yy(2:length(yy));
   end
return

