function [mat1, mat2, vclmn]=combinar_matrices(nproc)

    mat1 = loadmat('mat1_1');
    mat2 = loadmat('mat2_1');
    vclmn = loadmat('vsf_1');

    for i=2:nproc-1
        name_mat1 = strcat('mat1_',num2str(i));
        name_mat2 = strcat('mat2_',num2str(i));
        name_vclmn = strcat('vsf_',num2str(i));

        aux_mat1 = loadmat(name_mat1);

        mat1 = mat1 + aux_mat1;

        aux_mat2 = loadmat(name_mat2);
        mat2 = mat2 + aux_mat2;
        
        aux_vclmn = loadmat(name_vclmn);
        vclmn=[vclmn;aux_vclmn];
    end
end
