function [fxr]=m11_m11cma(a1,alfa1,a2,alfa2,a3,alfa3,a4,alfa4,orden)
global intqrec puntosx puntosr
if isempty(orden)
   orden=1;
end
if isempty(puntosx)
   puntosx=20;
end
if isempty(puntosr)
   puntosr=20;
end
limx=[0;1];
limr=[0;20];
a=a1-a2;
a=norm(a);
c=a3-a4;
c=norm(c);
intrec=0;
fxr=int11_11cma('fu11_11cma',limx,limr,puntosx,puntosr,...
    a1,alfa1,a2,alfa2,a3,alfa3,a4,alfa4,orden);
fxr=(4/pi)^(2)*(alfa1*alfa2*alfa3*alfa4)^(2.5)*a^5*c^5*fxr;
return
